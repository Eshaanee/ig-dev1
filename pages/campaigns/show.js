import React, {Component} from 'react';
import { Card } from 'semantic-ui-react';
import Layout from '../../components/Layout';
import Campaign from '../../ethereum/campaign';
class CampaignShow extends Component {

  static async getInitialProps(props) { //async function is used for calling
    const campaign= Campaign(props.query.address);
    const summary= await campaign.methods.getSummary().call();

    return{
      minimumContribution: summary[0],
      balance: summary[1],
      impactguru:summary[2],
      approverCount:summary[3],
      rejectorCount: summary[4],
      donarCount: summary[5]
    };

  }

  renderCards() {
    const {
      balance,
      impactguru,
      minimumContribution,
      approverCount,
      rejectorCount,
      donarCount

    } = this.props;
    const items = [
      
  {

header: minimumContribution,
description: '',
meta: 'Contributiin should be greater than 5 ImpactCoins',
style : {overflowWrap: 'break-word'}

},

{

header: balance,
description: '',
meta: 'Amount Collected',
style : {overflowWrap: 'break-word'}

},
{

header: approverCount,
description: '',
meta: 'Number of Approvals',
style : {overflowWrap: 'break-word'}

},
{

header: rejectorCount,
description: '',
meta:'Number of Rejectors',
style : {overflowWrap: 'break-word'}

},
{

header: donarCount,
description: '',
meta: 'Number of Donars',
style : {overflowWrap: 'break-word'}

},

  ];
    return <Card.Group items={items} />;
  }
render()  {
    return (
      <Layout>
      <h3>Campaigns Show</h3>
      {this.renderCards()}
      </Layout>
      );
  }
}
export default CampaignShow;

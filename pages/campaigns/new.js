import React, {Component} from 'react';
import Layout from '../../components/Layout';
import {Form, Message, Button} from 'semantic-ui-react';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
import {Router} from '../../routes';
const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
]

class CampaignNew extends Component {

  state ={
    minimumContribution:'',
    errormessage:'',
    loading: false
  }

  //onSubmit = this.onSubmit.bind(this);

  onSubmit=async event => {
    event.preventDefault();
    this.setState({loading: true, errormessage: ''});

  try{
    const accounts = await web3.eth.getAccounts();
    await factory.methods.
    createCampaign(this.state.minimumContribution)
    .send({
      from: accounts[0]
    });

    Router.pushRoute('/');
  }
  catch(err) {
    this.setState( {errormessage: err.message});
  }
  this.setState({loading: false});
  };

  render() {
    return (
      <Layout>
      <h1>Start a Fundraiser</h1>

      <Form onSubmit={this.onSubmit} >
       <Form.Group widths = 'equal'>
        <Form.Input fluid label ='First name' placeholder='First name'/>
        <Form.Input fluid label ='Last name' placeholder='Last name'/>
        <Form.Select fluid label ='Gender' options={options} placeholder='Gender'/>
       </Form.Group>

       <Form.Group>
        <Form.Input fluid label ='Donate' value={this.state.minimumContribution}
        onChange={event=> this.setState({minimumContribution: event.target.value })} />

       </Form.Group>
       <Form.TextArea label='Your Story' placeholder='Tell us about you..'/>
       <Form.Checkbox label='I agree to the Terms and Conditions'/>
       { this.state.errormessage !== '' && <Message
          color='red'
         header='There was some error with your submission'
         content ={this.state.errormessage}
       />}
       <Form.Button loading ={this.state.loading}>Create</Form.Button>
      </Form>


      </Layout>
    );
  }
}
export default CampaignNew;

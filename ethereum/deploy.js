const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./build/CampaignFactory.json');
const provider = new HDWalletProvider('start whip exact success tag method traffic traffic banner kangaroo unhappy earth', 'https://rinkeby.infura.io/v3/b9a93fbf348a48a8a3eb791f949eb73b');
const web3= new Web3(provider);
const deploy = async () => {

  const accounts = await web3.eth.getAccounts();
console.log('attempting to deploy from account', accounts[0]);
  const result= await new web3.eth.Contract(
    JSON.parse(compiledFactory.interface)
  ).deploy( { data: '0x'+compiledFactory.bytecode }).send( { gas: '1000000', from: accounts[0] });

  console.log('contracts deployed to', result.options.address);
};
deploy();

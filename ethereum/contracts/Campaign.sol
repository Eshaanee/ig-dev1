pragma solidity ^0.4.17;
contract CampaignFactory {
    address[] public deployedCampaigns;
    function createCampaign(uint minimum) public {
    address newCampaign=new Campaign( minimum, msg.sender);
    deployedCampaigns.push(newCampaign);
    }

    function getDeployedCampaigns() public view returns(address[]) {
        return deployedCampaigns;
    }
    address creator1 =msg.sender;
     function Creator() public view returns(address) {
        return creator1;
    }


}
contract Campaign  {

    struct Request {
        string purpose;
        uint value;
        address campaigner;
        bool complete;
        uint approvalCount;
        uint rejectorCount;
        mapping(address => bool) approvals;
        mapping(address => bool) rejectors;

    }

    Request[] public requests;
    address public impactguru;
    mapping(address => bool) donars;
    uint donarsCount=0;
    uint minContribution;
    uint approversCount=0;
    uint rejectorsCount=0;


    function Campaign(uint minimum, address creator) public {
        impactguru =  creator;
        minContribution= minimum;
    }

    function donate(uint  value) public payable {
        require(msg.value > minContribution);
        donars[msg.sender]=true;
        donarsCount++;
    }

    function addrequest(string purpose, uint value, address campaigner) public {

        Request memory newRequest = Request({
            purpose: purpose,
            value:  value,
            campaigner: campaigner,
            approvalCount:0,
            rejectorCount:0,
            complete: false
        });
        requests.push(newRequest);

    }

    function approveRequest(uint index) public {
        Request storage request = requests[index];

        require(!request.approvals[msg.sender]);
        request.approvals[msg.sender]= true;
        request.approvalCount++;
        approversCount++;

    }

     function rejectRequest(uint index) public {
        Request storage request = requests[index];

        require(!request.rejectors[msg.sender]);
        request.rejectors[msg.sender]= true;
        request.rejectorCount++;
        rejectorsCount++;
    }

    function finalizeRequest(uint index) public {
        Request storage request= requests[index];
        require(msg.sender == impactguru);
        require(request.approvalCount > (request.rejectorCount));
        require(!request.complete);
        request.complete = true;
        request.campaigner.transfer(request.value);
    }

    function getSummary() public view returns(uint,uint,address,uint,uint,uint) {
      return (
        minContribution,
        this.balance,
        impactguru,
        approversCount,
        rejectorsCount,
        donarsCount
        );
    }
}

import web3 from  './web3';
import CampaignFactory from './build/CampaignFactory.json';
const instance= new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  '0x37c8D073ac3EDe54b45098518720A5d72c260E7E'
);

//personal.unlockAccount(eth.accounts[0]);
//Unlock account 0x5739a3D258cCd5B2538cc8A923CD9aAF7c707007
//Passphrase:
//true

export default instance;

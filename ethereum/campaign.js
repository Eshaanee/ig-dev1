import web3 from './web3';
import Campaign from './build/Campaign.json';

export default(address) => { //address is a token present in the url(mentioned as :address in  route.js)
  return new web3.eth.Contract(
    JSON.parse(Campaign.interface),
    address
  );
};

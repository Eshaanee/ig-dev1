import Web3 from 'web3';//aim of web3.js is to configure web3 and provider.



let web3;

if(typeof window!== 'undefined' && typeof window.web3!=='undefined')
{
  //we are on browser and the user is running metamask.
  web3 = new Web3(window.web3.currentProvider);
}
else {
  //we are on next.js server and the user is not running metamask
  const provider = new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/b9a93fbf348a48a8a3eb791f949eb73b');
  web3 = new Web3(provider);
}

export default web3;

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _web = require('./web3');

var _web2 = _interopRequireDefault(_web);

var _CampaignFactory = require('./build/CampaignFactory.json');

var _CampaignFactory2 = _interopRequireDefault(_CampaignFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var instance = new _web2.default.eth.Contract(JSON.parse(_CampaignFactory2.default.interface), '0x37c8D073ac3EDe54b45098518720A5d72c260E7E');

//personal.unlockAccount(eth.accounts[0]);
//Unlock account 0x5739a3D258cCd5B2538cc8A923CD9aAF7c707007
//Passphrase:
//true

exports.default = instance;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV0aGVyZXVtXFxmYWN0b3J5LmpzIl0sIm5hbWVzIjpbIndlYjMiLCJDYW1wYWlnbkZhY3RvcnkiLCJpbnN0YW5jZSIsImV0aCIsIkNvbnRyYWN0IiwiSlNPTiIsInBhcnNlIiwiaW50ZXJmYWNlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxBQUFPLEFBQVAsQUFBa0IsQUFBbEI7Ozs7QUFDQSxBQUFPLEFBQVAsQUFBNEIsQUFBNUI7Ozs7OztBQUNBLElBQU0sV0FBVSxJQUFJLGNBQUssQUFBTCxJQUFTLEFBQWIsU0FDZCxLQUFLLEFBQUwsTUFBVywwQkFBZ0IsQUFBM0IsQUFEYyxZQUVkLEFBRmMsQUFBaEI7O0FBS0E7QUFDQTtBQUNBO0FBQ0EsQUFFQTs7a0JBQWUsQUFBZiIsImZpbGUiOiJmYWN0b3J5LmpzIiwic291cmNlUm9vdCI6IkQ6L0JMT0NLQ0hBSU4va2lja3N0YXJ0In0=
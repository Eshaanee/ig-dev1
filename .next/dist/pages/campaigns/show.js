'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _Layout = require('../../components/Layout');

var _Layout2 = _interopRequireDefault(_Layout);

var _campaign = require('../../ethereum/campaign');

var _campaign2 = _interopRequireDefault(_campaign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'D:\\BLOCKCHAIN\\kickstart\\pages\\campaigns\\show.js?entry';


var CampaignShow = function (_Component) {
  (0, _inherits3.default)(CampaignShow, _Component);

  function CampaignShow() {
    (0, _classCallCheck3.default)(this, CampaignShow);

    return (0, _possibleConstructorReturn3.default)(this, (CampaignShow.__proto__ || (0, _getPrototypeOf2.default)(CampaignShow)).apply(this, arguments));
  }

  (0, _createClass3.default)(CampaignShow, [{
    key: 'renderCards',
    value: function renderCards() {
      var _props = this.props,
          balance = _props.balance,
          impactguru = _props.impactguru,
          minimumContribution = _props.minimumContribution,
          approverCount = _props.approverCount,
          rejectorCount = _props.rejectorCount,
          donarCount = _props.donarCount;

      var items = [{

        header: minimumContribution,
        description: '',
        meta: 'Contributiin should be greater than 5 ImpactCoins',
        style: { overflowWrap: 'break-word' }

      }, {

        header: balance,
        description: '',
        meta: 'Amount Collected',
        style: { overflowWrap: 'break-word' }

      }, {

        header: approverCount,
        description: '',
        meta: 'Number of Approvals',
        style: { overflowWrap: 'break-word' }

      }, {

        header: rejectorCount,
        description: '',
        meta: 'Number of Rejectors',
        style: { overflowWrap: 'break-word' }

      }, {

        header: donarCount,
        description: '',
        meta: 'Number of Donars',
        style: { overflowWrap: 'break-word' }

      }];
      return _react2.default.createElement(_semanticUiReact.Card.Group, { items: items, __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_Layout2.default, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        }
      }, _react2.default.createElement('h3', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        }
      }, 'Campaigns Show'), this.renderCards());
    }
  }], [{
    key: 'getInitialProps',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(props) {
        var campaign, summary;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                //async function is used for calling
                campaign = (0, _campaign2.default)(props.query.address);
                _context.next = 3;
                return campaign.methods.getSummary().call();

              case 3:
                summary = _context.sent;
                return _context.abrupt('return', {
                  minimumContribution: summary[0],
                  balance: summary[1],
                  impactguru: summary[2],
                  approverCount: summary[3],
                  rejectorCount: summary[4],
                  donarCount: summary[5]
                });

              case 5:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getInitialProps(_x) {
        return _ref.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return CampaignShow;
}(_react.Component);

exports.default = CampaignShow;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzXFxjYW1wYWlnbnNcXHNob3cuanMiXSwibmFtZXMiOlsiUmVhY3QiLCJDb21wb25lbnQiLCJDYXJkIiwiTGF5b3V0IiwiQ2FtcGFpZ24iLCJDYW1wYWlnblNob3ciLCJwcm9wcyIsImJhbGFuY2UiLCJpbXBhY3RndXJ1IiwibWluaW11bUNvbnRyaWJ1dGlvbiIsImFwcHJvdmVyQ291bnQiLCJyZWplY3RvckNvdW50IiwiZG9uYXJDb3VudCIsIml0ZW1zIiwiaGVhZGVyIiwiZGVzY3JpcHRpb24iLCJtZXRhIiwic3R5bGUiLCJvdmVyZmxvd1dyYXAiLCJyZW5kZXJDYXJkcyIsImNhbXBhaWduIiwicXVlcnkiLCJhZGRyZXNzIiwibWV0aG9kcyIsImdldFN1bW1hcnkiLCJjYWxsIiwic3VtbWFyeSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLEFBQU8sQUFBUTs7OztBQUNmLEFBQVM7O0FBQ1QsQUFBTyxBQUFZOzs7O0FBQ25CLEFBQU8sQUFBYzs7Ozs7Ozs7O0lBQ2YsQTs7Ozs7Ozs7Ozs7a0NBaUJVO21CQVNSLEtBVFEsQUFTSDtVQVRHLEFBRVYsaUJBRlUsQUFFVjtVQUZVLEFBR1Ysb0JBSFUsQUFHVjtVQUhVLEFBSVYsNkJBSlUsQUFJVjtVQUpVLEFBS1YsdUJBTFUsQUFLVjtVQUxVLEFBTVYsdUJBTlUsQUFNVjtVQU5VLEFBT1Ysb0JBUFUsQUFPVixBQUdGOztVQUFNOztnQkFFUixBQUVNLEFBQ1I7cUJBSEUsQUFHVyxBQUNiO2NBSkUsQUFJSSxBQUNOO2VBQVEsRUFBQyxjQVBTLEFBRWhCLEFBS00sQUFBZTs7QUFMckIsQUFFRixPQUprQjs7Z0JBV2xCLEFBRVEsQUFDUjtxQkFIQSxBQUdhLEFBQ2I7Y0FKQSxBQUlNLEFBQ047ZUFBUSxFQUFDLGNBaEJTLEFBV2xCLEFBS1EsQUFBZTs7QUFMdkIsQUFFQTs7Z0JBTUEsQUFFUSxBQUNSO3FCQUhBLEFBR2EsQUFDYjtjQUpBLEFBSU0sQUFDTjtlQUFRLEVBQUMsY0F4QlMsQUFtQmxCLEFBS1EsQUFBZTs7QUFMdkIsQUFFQTs7Z0JBTUEsQUFFUSxBQUNSO3FCQUhBLEFBR2EsQUFDYjtjQUpBLEFBSUssQUFDTDtlQUFRLEVBQUMsY0FoQ1MsQUEyQmxCLEFBS1EsQUFBZTs7QUFMdkIsQUFFQTs7Z0JBTUEsQUFFUSxBQUNSO3FCQUhBLEFBR2EsQUFDYjtjQUpBLEFBSU0sQUFDTjtlQUFRLEVBQUMsY0F4Q0wsQUFBYyxBQW1DbEIsQUFLUSxBQUFlLEFBS25COztBQVZKLEFBRUE7MkNBUVcsQUFBQyxzQkFBRCxBQUFNLFNBQU0sT0FBWixBQUFtQjtvQkFBbkI7c0JBQVAsQUFBTyxBQUNSO0FBRFE7T0FBQTs7Ozs2QkFFRCxBQUNOOzZCQUNFLEFBQUM7O29CQUFEO3NCQUFBLEFBQ0E7QUFEQTtBQUFBLE9BQUEsa0JBQ0EsY0FBQTs7b0JBQUE7c0JBQUE7QUFBQTtBQUFBLFNBREEsQUFDQSxBQUNDLHdCQUhILEFBQ0UsQUFFQyxBQUFLLEFBR1Q7Ozs7OzJHLEFBL0U0Qjs7Ozs7bUJBQVM7QUFDOUI7QSwyQkFBVSx3QkFBUyxNQUFBLEFBQU0sTUFBZixBQUFxQixBOzt1QkFDaEIsU0FBQSxBQUFTLFFBQVQsQUFBaUIsYUFBakIsQUFBOEIsQTs7bUJBQTdDO0E7O3VDQUdpQixRQURqQixBQUNpQixBQUFRLEFBQzdCOzJCQUFTLFFBRkwsQUFFSyxBQUFRLEFBQ2pCOzhCQUFXLFFBSFAsQUFHTyxBQUFRLEFBQ25CO2lDQUFjLFFBSlYsQUFJVSxBQUFRLEFBQ3RCO2lDQUFlLFFBTFgsQUFLVyxBQUFRLEFBQ3ZCOzhCQUFZLFFBTlIsQUFNUSxBQUFRLEE7QUFOaEIsQUFDSjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVBxQixBLEFBbUYzQjs7a0JBQUEsQUFBZSIsImZpbGUiOiJzaG93LmpzP2VudHJ5Iiwic291cmNlUm9vdCI6IkQ6L0JMT0NLQ0hBSU4va2lja3N0YXJ0In0=